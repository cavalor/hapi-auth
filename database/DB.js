const _models = new WeakMap()
const _sequelize = new WeakMap()

export default class DB {
  constructor (sequelize) {
    _models.set(this, {})
    _sequelize.set(this, sequelize)
  }

  /**
   * Return all models defined in the database instance context
   * @return {Object} models list
   */
  get models () {
    return _models.get(this)
  }

  /**
   * Return the sequelize instance
   * @return {Sequelize}
   */
  get sequelize () {
    return _sequelize.get(this)
  }

  /**
   * Set a model in the "models" map
   * @param {Sequelize.Model}
   */
  setModel (model) {
    this.models[model.name] = model
  }

  /**
   * Get a model by name
   * @return {Sequelize.Model}
   */
  getModel (name) {
    return this.models.hasOwnProperty(name) ? this.models[name] : null
  }
}

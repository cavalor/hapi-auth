import fs from 'fs'
import path from 'path'
import Sequelize from 'sequelize'
import DB from './DB'
import config from '../config/database.json'

// Extract database configuration information
const { username, password, database, host } = config.development

// Create URI string from previous information
const uri = `postgres://${username}:${password}@${host}:5432/${database}`

// Create sequelize instance
const sequelize = new Sequelize(uri, {
  // logging: null
})

// Instantiate database api
const db = new DB(sequelize)

// Resolve entity descriptor file from filename
const resolve = file => path.resolve(__dirname, 'models', file)

// Read models directory and return each contained file name
const modelDescriptors = fs.readdirSync(path.join(__dirname, 'models'))

// Define an array to store the model.sync() promises
const syncPromises = []

// Loop over model descriptors
for (const descriptor of modelDescriptors) {
  const model = sequelize.import(resolve(descriptor))
  // Add the sync promise in array
  syncPromises.push(model.sync())
}

// Execute all model.sync() promises
Promise.all(syncPromises).then((models) => {
  for (const model of models) {
    // Set entity relations
    if (model.associate) {
      model.associate(models)
    }
    // Register model in DB instance
    db.setModel(model)
  }
}).catch((err) => {
  throw err
})

// Export hapi plugin
export const plugin = {
  register (server, options, next) {
    console.log('Register database plugin')

    // Decorate request and server objects with DB instance
    server.decorate('request', 'db', db)
    server.decorate('server', 'db', db)

    // Decorate request and server objects with models array
    server.decorate('request', 'models', db.models)
    server.decorate('server', 'models', db.models)

    // Decorate request and server objects with getModel method
    server.decorate('request', 'getModel', db.getModel)
    server.decorate('server', 'getModel', db.getModel)

    next()
  }
}

// Set the plugins attributes
plugin.register.attributes = {
  name: 'database',
  versions: '1.0.0'
}

// Export database as default
export default db

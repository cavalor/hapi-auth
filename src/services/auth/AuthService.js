import bcrypt from 'bcrypt'
import boom from 'boom'
import jwt from 'jsonwebtoken'
import { getUser } from './utils'

const secret = 'secretString'

function createJwt (user, groups = []) {
  return jwt.sign({
    id: user.id,
    username: user.username,
    email: user.email,
    scope: 'auth',
    groups,
  }, secret, { algorithm: 'HS256', expiresIn: "1h" } )
}

function hashPassword(password) {
  // Generate a salt at level 10 strength
  return bcrypt.hash(password, 10)
}

const _server = new WeakMap()
export default class AuthService {
  attributes = ['id', 'username', 'email']

  constructor (server) {
    _server.set(this, server)
  }

  get users () {
    return _server.get(this).getModel('User')
  }

  listUsers () {
    return this.users.findAll({
      attributes: this.attributes
    })
  }

  registerUser (data) {
    return hashPassword(data.password).then((hash) => {
      data.password = hash
      return this.users.create(data)
    })
  }

  authenticateUser (data) {
    return getUser(data).then((user) => {
      if (user) {
        console.log(user.username, user.email)
        return bcrypt.compare(data.password, user.password).then((isValid) => {
          if (isValid) return user
        })
      }
      return null
    }).then((validUser) => {
      console.log('is valid user', !!validUser)
      if (!validUser) {
        throw boom.unauthorized()
      }
      return { token: createJwt(validUser) }
    })
  }
}

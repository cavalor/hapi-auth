import AuthService from './AuthService'
import { verifyUniqueUser } from './utils'
import { authenticateSchema, registerSchema } from './schemas'

export default function register (server) {
  const auth = new AuthService(server)

  server.route({
    method: 'GET',
    path: '/auth',
    config: {
      handler (req, res) {
        auth.listUsers().then((data) => {
          res(data)
        })
      },
      // Add authentication to this route
      // The user must have a scope of `admin`
      auth: {
        strategy: 'jwt',
        scope: ['admin', 'auth']
      }
    }
  })

  server.route({
    method: 'POST',
    path: '/auth/register',
    config: {
      handler (req, res) {
        auth.registerUser(req.payload).then((data) => {
          res(data).code(201)
        }).catch((err) => {
          throw err
        })
      },
      pre: [
        verifyUniqueUser
      ],
      validate: {
        payload: registerSchema
      }
    }
  })

  server.route({
    method: 'POST',
    path: '/auth/login',
    handler (req, res) {
      auth.authenticateUser(req.payload).then((data) => {
        res(data).code(201)
      }).catch((err) => {
        res(err)
      })
    },
    config: {
      validate: {
        payload: authenticateSchema
      }
    }
  })

  return {
    name: 'auth',
    api: auth
  }
}

import joi from 'joi'

export const registerSchema = joi.object({
  username: joi.string().required(),
  email: joi.string().email().required(),
  password: joi.string().required()
})

export const authenticateSchema = joi.alternatives().try(
  joi.object({
    username: joi.string().min(2).max(30).required(),
    email: joi.string().email().required(),
    password: joi.string().required()
  }),
  joi.object({
    email: joi.string().email().required(),
    password: joi.string().required()
  })
);

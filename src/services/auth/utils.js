import boom from 'boom'
import db from '../../../database'

export function getUser ({ email, username }) {
  const User = db.getModel('User')
  return User.find({
    where: {
      $or: [{ email }, { username }]
    }
  })
}

// Find an entry from the database that matches either the email or username
export function verifyUniqueUser(req, res) {
  getUser(req.payload).then((user) => {
    if (user) {
      if (user.username === req.payload.username) {
        res(boom.badRequest('Username taken'));
      }
      if (user.email === req.payload.email) {
        res(boom.badRequest('Email taken'));
      }
    }
    res(req.payload)
  })
}

export function authenticateUser (req, res) {
 res('ok')
}

import fs from 'fs'
import path from 'path'

const services = {}

function registerServices (server) {
  const register = (file) => {
    const RegisterService = require(path.resolve(__dirname, file)).default
    const service = RegisterService(server)
    services[service.name] = service.api
  }

  const filter = file => ((file.indexOf('.') !== 0)
    && (file.slice(-3) !== '.js'))

  fs.readdirSync(path.resolve(__dirname))
    .filter(filter)
    .map(register)
}

function getService (name) {
  return services.hasOwnProperty(name) ? services[name] : null
}

const servicesPlugin = {
  register (server, options, next) {
    console.log('Register service plugin')

    registerServices(server)

    server.decorate('request', 'service', getService)
    server.decorate('server', 'service', getService)

    next()
  }
}

servicesPlugin.register.attributes = {
  name: 'services',
  version: '1.0.0'
}

export default servicesPlugin

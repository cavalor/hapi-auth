import hapi from 'hapi'
import hapiAuthJwt from 'hapi-auth-jwt'
import blipp from 'blipp'
import { plugin as database } from '../database'
import services from './services'
import corsHeaders from './middlewares/cors'

const jwtSecret = 'secretString'

const server = new hapi.Server()
server.connection({
  port: 3000
})

server.register([
  database,
  hapiAuthJwt,
  { register: blipp, options: {} }
], (err) => {
  if (err) {
    throw err
  }
  // We're giving the strategy both a name
  // and scheme of 'jwt'
  server.auth.strategy('jwt', 'jwt', {
    key: jwtSecret,
    verifyOptions: { algorithms: ['HS256'] }
  })
  server.register(services)
  server.ext('onPreResponse', corsHeaders)
  server.route({
    method: 'GET',
    path: '/',
    handler (request, reply) {
      reply('test')
    }
  })
  server.start()
})
